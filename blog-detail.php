<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= Blog ?></title>
    <?php include "topheader.php"; ?>

    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/pattern-4.png);">
        <!--End Page Info-->
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Our Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <div class="image">
                                <img src="images/resource/news-11.jpg" alt="" />
                            </div>
                            <div class="lower-content">
                                <ul class="post-meta">
                                    <li><span class="icon fa fa-calendar"></span>February 26th, 2018</li>
                                    <li><span class="icon fa fa-user"></span>Roger Jenina</li>
                                    <li><span class="icon fa fa-folder"></span>Email Marketing</li>
                                </ul>
                                <h3>The Solution-Focused:  How Emotional Intelligence Can Make You a Stronger Leader.</h3>
                                <div class="text">
                                	<p>Must explain to you how all this mistaken idea of denouncing pleasure and praising pain was & born and I will give you a complete account of the system, and expound the actual teachings of  explorer of the truth, the master-builder of human happiness Explain to you how all mistaken pleasure and praising pain was born and we will give you a complete.</p>
                                    <p>Account of the system, and expound the actual teachings of the explorer of the truth, the master-builder of human happiness which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes.</p>
                                    <blockquote>
                                        <div class="quote-icon"><span class="icon flaticon-right-quotation-sign"></span></div>
                                        <div class="blockquote-text">  There anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which  can procure him some great pleasure. </div>
                                        <div class="quote-info">
                                            <h4>Teena Venanda</h4>
                                            <div class="quote-desination">CEO/Founder</div>
                                        </div>
                                    </blockquote>
                                    <div class="two-column clearfix">
                                        <div class="row clearfix">
                                            <div class="image-column col-md-6 col-sm-6 col-xs-12">
                                                <div class="image">
                                                    <img src="images/resource/news-14.jpg" alt="" />
                                                </div>
                                            </div>
                                            <div class="text-column col-md-6 col-sm-6 col-xs-12">
                                            	<p>Explain you how  mistaken idea denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the  of truth pain itself occur great.</p>
                                                <p>The master-builder of human happiness which toil and pain can procure him some seds great pleasure take a of ever.</p>
                                            </div>
										</div>
                                    </div>
                                    
                                    <!--post-share-options-->
                                    <div class="post-share-options">
                                        <div class="post-share-inner clearfix">
                                            <ul class="pull-left info-links clearfix">
                                                <li><a href="#"><span class="fa fa-heart"></span></a>18</li>
                                                <li><a href="#"><span class="fa fa-comments"></span></a>6</li>
                                            </ul>
                                            <div class="pull-right tags"><span>Tags: </span><a href="#">idea</a>, <a href="#">services</a>, <a href="#">Growth</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="group-title">
                        	<h2>About Author</h2>
                        </div>
                        
                        <!--Author Box-->
                        <div class="author-box">
                            <div class="author-comment">
                                <div class="inner">
                                    <div class="image"><img src="images/resource/author-6.jpg" alt="" /></div>
                                    <h3>Lilly Anderson</h3>
                                    <div class="text">We will give you a complete account of the system, and expound actual teachings of explorer of the truth, the master-builder of human happiness.</div>
                                    <ul class="social-icon-two">
                                        <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                        <li><a href="#"><span class="fa fa-youtube"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Comments Area-->
                        <div class="comments-area">
                            <div class="group-title">
                            	<h2>Read Comments</h2>
                                <div class="separator"></div>
                            </div>
                            <!--Comment Box-->
                            <div class="comment-box">
                                <div class="comment">
                                    <div class="author-thumb"><img src="images/resource/author-7.jpg" alt=""></div>
                                    <div class="comment-inner">
                                        <div class="comment-info clearfix">Steven Rich – Sep 17, 2016:</div>
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star light"></span>
                                        </div>
                                        <div class="text">How all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings.</div>
                                    </div>
                                </div>
                            </div>
                            <!--Comment Box-->
                            <div class="comment-box reply-comment">
                                <div class="comment">
                                    <div class="author-thumb"><img src="images/resource/author-8.jpg" alt=""></div>
                                    <div class="comment-inner">
                                        <div class="comment-info clearfix">William Cobus – Aug 21, 2016:</div>
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-half-empty"></span>
                                        </div>
                                        <div class="text">There anyone who loves or pursues or desires to obtain pain itself, because it is pain sed, because occasionally circumstances occur some great pleasure.</div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <!-- Comment Form -->
                        <div class="comment-form">	
                            <div class="group-title">
                            	<h2>Add Your Comments</h2>
                                <div class="separator"></div>
                            </div>
                            <div class="rating-box">
                                <div class="text"> Your Rating:</div>
                                <div class="rating">
                                    <a href="#"><span class="fa fa-star"></span></a>
                                </div>
                                <div class="rating">
                                    <a href="#">
                                    	<span class="fa fa-star"></span>
                                    	<span class="fa fa-star"></span>
                                    </a>
                                </div>
                                <div class="rating">
                                    <a href="#">
                                    	<span class="fa fa-star"></span>
                                    	<span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </a>
                                </div>
                                <div class="rating">
                                    <a href="#">
                                    	<span class="fa fa-star"></span>
                                    	<span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </a>
                                </div>
                                <div class="rating">
                                    <a href="#">
                                    	<span class="fa fa-star"></span>
                                    	<span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </a>
                                </div>
                            </div>
                            <form method="post" action="contact.php">
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <label>First Name*</label>
                                        <input type="text" name="username" placeholder="" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <label>Last Name*</label>
                                        <input type="text" name="username" placeholder="" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>Email*</label>
                                        <input type="email" name="email" placeholder="" required>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>Your Comments*</label>
                                        <textarea name="message" placeholder=""></textarea>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">Post Comment</button>
                                    </div>
                                    
                                </div>
                            </form>
                                
                        </div>
                        
                        
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar with-border">
					
                    	<!-- Search -->
                        <div class="sidebar-widget search-box">
                        	<form method="post" action="contact.php">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Enter Your Keyword..." required>
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
						</div>
                        
                        <!--Blog Category Widget-->
                        <div class="sidebar-widget sidebar-blog-category">
                            <div class="sidebar-title">
                                <h2>Categories</h2>
                            </div>
                            <ul class="blog-cat">
                                <li><a href="#">Business  <span>(6)</span></a></li>
                                <li><a href="#">Email Marketing <span>(2)</span></a></li>
                                <li><a href="#">PPC Management <span>(8)</span></a></li>
                                <li><a href="#">Social Marketing <span>(5)</span></a></li>
                                <li><a href="#">Uncategorized <span>(3)</span></a></li>
                            </ul>
                        </div>
                        
                        <!--Recent Comments-->
                        <div class="sidebar-widget recent-comments">
                            <div class="sidebar-title">
                                <h2>Recent Comments</h2>
                            </div>
                            
                            <div class="comment-block">
                            	<div class="inner">
                                	<div class="date">August 17, 2017</div>
                                    <div class="content">
                                    	<div class="image"><img src="images/resource/author-4.jpg" alt="" /></div>
                                        <h3><a href="#">Mark Mitchael</a></h3>
                                        <div class="text">It is long established fact</div>
                                        <a href="#" class="reply-btn"><span class="fa fa-mail-forward"></span></a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="comment-block">
                            	<div class="inner">
                                	<div class="date">July 08, 2017</div>
                                    <div class="content">
                                    	<div class="image"><img src="images/resource/author-5.jpg" alt="" /></div>
                                        <h3><a href="#">Steve Bowerman</a></h3>
                                        <div class="text">When looking at its layout</div>
                                        <a href="#" class="reply-btn"><span class="fa fa-mail-forward"></span></a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    
                    	<!-- Popular Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h2>Popular Posts</h2></div>

                            <article class="post">
                            	<figure class="post-thumb"><img src="images/resource/post-thumb-1.jpg" alt=""><a class="overlay" href="blog-single.php"></a></figure>
                                <div class="text"><a href="blog-single.php">6 ways to keep in control your debtors</a></div>
                                <div class="post-info">April 18, 2017</div>
                            </article>
                            
                            <article class="post">
                            	<figure class="post-thumb"><img src="images/resource/post-thumb-2.jpg" alt=""><a class="overlay" href="blog-single.php"></a></figure>
                                <div class="text"><a href="blog-single.php">What a finance director could add business</a></div>
                                <div class="post-info">February 14, 2017</div>
                            </article>
                            
                            <article class="post">
                            	<figure class="post-thumb"><img src="images/resource/post-thumb-3.jpg" alt=""><a class="overlay" href="blog-single.php"></a></figure>
                                <div class="text"><a href="blog-single.php">Tips for sucess in 2016: Ways to grow your business</a></div>
                                <div class="post-info">January 16, 2017</div>
                            </article>
                            
						</div>
                        
                        <!-- Popular Tags -->
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title"><h2>Tag Cloud</h2></div>
                            <a href="#">Idea</a>
                            <a href="#">Finance</a>
                            <a href="#">Experts</a>
                            <a href="#">Tips</a>
                            <a href="#">Marketing</a>
                            <a href="#">Services</a>
                            <a href="#">Growth</a>
                        </div>
                    
                    </aside>
                </div>
                
            </div>
        </div>
    </div>

<?php include "footer.php"; ?>