<!--Main Footer-->
<footer class="main-footer">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
                <div class="row clearfix">
                <!--Column-->
                <div class="column col-md-7 col-sm-12 col-xs-12">
                <div class="footer-widget links-widget">
                <div class="footer-title">
                    <h2><?=Useful_Links ?></h2>
                </div>
                <div class="widget-content">
                    <div class="row clearfix">
                        <div class="widget-column col-md-6 col-sm-6 col-xs-12">
                            <ul class="footer-list">
                                <li><a href="crypto_marketing_company.php<?=$language ?>"><?= Company ?></a></li>
                                <li><a href="crypto_development_services.php<?=$language ?>"><?= Development ?></a></li>
                                <li><a href="crypto_marketing_services.php<?=$language ?>"><?= Marketing ?></a></li>
                                <li><a href="crypto_advisor.php<?=$language ?>"><?= Crypto_Advisor ?></a></li>
                                <li><a href="market_making.php<?=$language ?>"><?= Market_Making ?></a></li>
                            </ul>
                        </div>
                        <div class="widget-column col-md-6 col-sm-6 col-xs-12">
                            <ul class="footer-list">
                                <li><a href="legal_assistance.php<?=$language ?>"><?= LegalAssistance ?></a></li>
                                <li><a href="exchanges_listing.php<?=$language ?>"><?= Exchanges ?></a></li>
                                <li><a href="ico_launching_services.php<?=$language ?>"><?= ICO_STO_Launching ?></a></li>
                                <li><a href="blog.php<?=$language ?>"><?= Blog ?></a></li>
                                <li><a href="contact.php<?=$language ?>"><span class="icon"></span><?=ContactUs ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                <!--Column-->
                <div class="column col-md-5 col-sm-12 col-xs-12">
                <div class="footer-widget office-widget">
                <div class="footer-title">
                    <h2><?=Corporate_Office ?></h2>
                </div>
                <div class="widget-content">
                    <div class="single-item-carousel owl-carousel owl-theme">
                        <div class="office-info">
                            <ul>
                                <li><strong><?=Addresslabel ?></strong></li>
                                <li>DUBAI, Business Bay, One by Omniyat Tower, office -1104 </li>
                                <li><a href="index.php#map-home"><?=Find_Map ?></a></li>
                                <li><strong><?=Telephonelabel ?></strong></li>
                                <li>+971 4 874 8165</li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--Copyright-->
    <div class="copyright">&copy; tokenncoin.com 2019. All Rights Reserved.</div>
</footer>
<!--Main Footer-->


</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>

<script src="js/jquery.js"></script>
<!--Revolution Slider-->
<script src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="js/main-slider-script.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/knob.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>

<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="js/map-script.js"></script>
<!--End Google Map APi-->

</body>
</html>