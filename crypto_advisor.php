<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title><?= Crypto_Advisor ?></title>
    <?php include "topheader.php"; ?>
 	
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/CRYPTO-ADVISOR.jpg);">
    </section>
    <!--End Page Title-->

    <!--We Are Section-->
    <section class="we-are-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title centered">
                            <p class="icon"><img src="images/svg/ico_advisor.svg" width="200px;" alt="" /></p>
                            <h2><?= Crypto_Advisor ?></h2>
                            <div class="text" style="text-align: left;"><?=Crypto_Advisor_info ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End We Are Section-->

<?php include "footer.php"; ?>