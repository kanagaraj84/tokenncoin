<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <title><?= title ?></title>
<style>
    .inner-box {
        height: 270px;
    }
</style>
 <?php include "topheader.php"; ?>

 <!-- Preloader -->
 <div class="preloader"></div>

 <!-- Main Header / Header Style Five-->
 <header class="main-header header-style-two">
   <?php include "header.php"; ?>
 </header>
 <style type="text/css">
  .faq-img{
  height: 400px;
}

@media only screen and (max-width: 480px) {

  .faq-img{
    height: 200px;
  }
}
</style>

 <!--End Main Header -->

 <!--Main Slider-->
 <section class="main-slider">

   <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_two_wrapper" data-source="gallery">
    <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1">
     <ul>

      <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-1.jpg" data-title="Slide Title" data-transition="parallaxvertical">
       <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-1.jpg"> 

       <div class="tp-caption" 
       data-paddingbottom="[0,0,0,0]"
       data-paddingleft="[0,0,0,0]"
       data-paddingright="[0,0,0,0]"
       data-paddingtop="[0,0,0,0]"
       data-responsive_offset="on"
       data-type="text"
       data-height="none"
       data-width="['560','700','700','550']"
       data-whitespace="normal"
       data-hoffset="['15','15','15','15']"
       data-voffset="['-100','-90','-90','-90']"
       data-x="['left','left','left','left']"
       data-y="['middle','middle','middle','middle']"
       data-textalign="['top','top','top','top']"
       data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
       <h2 class="light"><?=HomeSlide1_c1 ?></h2>
     </div>

     <div class="tp-caption" 
     data-paddingbottom="[0,0,0,0]"
     data-paddingleft="[0,0,0,0]"
     data-paddingright="[0,0,0,0]"
     data-paddingtop="[0,0,0,0]"
     data-responsive_offset="on"
     data-type="text"
     data-height="none"
     data-width="['470','700','700','550']"
     data-whitespace="normal"
     data-hoffset="['15','15','15','15']"
     data-voffset="['-30','40','20','0']"
     data-x="['left','left','left','left']"
     data-y="['middle','middle','middle','middle']"
     data-textalign="['top','top','top','top']"
     data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
     <div class="text light"><br /><br /><?=HomeSlide1_c2 ?></div>
   </div>

   <div class="tp-caption tp-resizeme" 
   data-paddingbottom="[0,0,0,0]"
   data-paddingleft="[0,0,0,0]"
   data-paddingright="[0,0,0,0]"
   data-paddingtop="[0,0,0,0]"
   data-responsive_offset="on"
   data-type="text"
   data-height="none"
   data-width="['560','700','700','550']"
   data-whitespace="normal"
   data-hoffset="['15','15','15','15']"
   data-voffset="['60','150','120','100']"
   data-x="['left','left','left','left']"
   data-y="['middle','middle','middle','middle']"
   data-textalign="['top','top','top','top']"
   data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
   <div class="btns-box">
    <a href="crypto_marketing_company.php" class="theme-btn btn-style-one"> <span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
  </div>
</div>

<div class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="shape"
data-height="auto"
data-whitespace="nowrap"
data-width="none"
data-hoffset="['15','15','15','15']"
data-voffset="['-60','15','15','15']"
data-x="['right','right','right','right']"
data-y="['middle','middle','middle','middle']"
data-frames='[{"from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<figure class="content-image"><img src="images/main-slider/content-image-1.png" alt=""></figure>
</div>

</li>

<li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
 <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-2.jpg"> 

 <div class="tp-caption" 
 data-paddingbottom="[0,0,0,0]"
 data-paddingleft="[0,0,0,0]"
 data-paddingright="[0,0,0,0]"
 data-paddingtop="[0,0,0,0]"
 data-responsive_offset="on"
 data-type="text"
 data-height="none"
 data-width="['560','700','700','550']"
 data-whitespace="normal"
 data-hoffset="['15','15','15','15']"
 data-voffset="['-90','-50','-50','-50']"
 data-x="['right','right','right','left']"
 data-y="['middle','middle','middle','middle']"
 data-textalign="['top','top','top','top']"
 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
 <h2 class="light"><?=HomeSlide2_c1 ?></h2>


</div>

<div class="tp-caption" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['560','700','700','550']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['-20','20','20','15']"
data-x="['right','right','right','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<div class="text light"><?=HomeSlide2_c2 ?></div>
</div>

<div class="tp-caption tp-resizeme" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['560','700','700','550']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['70','100','100','100']"
data-x="['right','right','right','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<div class="btns-box">
 <a href="crypto_marketing_services.php" class="theme-btn btn-style-one"> <span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
</div>
</div>

<div class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="shape"
data-height="auto"
data-whitespace="nowrap"
data-width="none"
data-hoffset="['15','15','15','15']"
data-voffset="['-60','0','0','0']"
data-x="['left','left','left','left']"
data-y="['middle','middle','middle','middle']"
data-frames='[{"from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<figure class="content-image"><img src="images/main-slider/content-image-2.png" alt=""></figure>
</div>

</li>

<li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1690" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-3.jpg" data-title="Slide Title" data-transition="parallaxvertical">
 <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-3.jpg"> 

 <div class="tp-caption" 
 data-paddingbottom="[0,0,0,0]"
 data-paddingleft="[0,0,0,0]"
 data-paddingright="[0,0,0,0]"
 data-paddingtop="[0,0,0,0]"
 data-responsive_offset="on"
 data-type="text"
 data-height="none"
 data-width="['560','700','700','550']"
 data-whitespace="normal"
 data-hoffset="['15','15','15','15']"
 data-voffset="['-100','-90','-90','-90']"
 data-x="['left','left','left','left']"
 data-y="['middle','middle','middle','middle']"
 data-textalign="['top','top','top','top']"
 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
 <h2 class="light"><?=HomeSlide3_c1 ?></h2>
</div>

<div class="tp-caption" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['470','700','700','550']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['-30','40','20','0']"
data-x="['left','left','left','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<div class="text light"><?=HomeSlide3_c2 ?></div>
</div>

<div class="tp-caption tp-resizeme" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['560','700','700','550']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['60','150','120','100']"
data-x="['left','left','left','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<div class="btns-box">
 <a href="crypto_development_services.php" class="theme-btn btn-style-one"> <span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
</div>
</div>

<div class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="shape"
data-height="auto"
data-whitespace="nowrap"
data-width="none"
data-hoffset="['-60','15','15','15']"
data-voffset="['0','0','0','0']"
data-x="['right','right','right','right']"
data-y="['bottom','bottom','bottom','bottom']"
data-frames='[{"from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<figure class="content-image"><img src="images/main-slider/content-image-3.png" alt=""></figure>
</div>

</li>

</ul>
</div>
</div>
</section>
<!--End Main Slider-->

<!--Featured Section-->
<section class="featured-section">
 <div class="auto-container">
  <div class="inner-container clearfix">

   <!--Featured Block-->
   <div class="featured-block col-md-4 col-sm-6 col-xs-12">
     <div class="inner-box">
      <div class="upper-box">
       <div class="text"><?=company_info ?>
       </div>
       <a class="read-more" href="crypto_marketing_company.php"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
     </div>
     <div class="lower-box">
       <div class="lower-inner">
        <div class="icon-box">
         <span class="icon"><img src="images/svg/ico_company.svg" alt="" /></span>
       </div>
       <h3><a href="crypto_marketing_company.php" class="textlinks" style="color:#ff4800;"><?=Company ?></a></h3>
       <div class="title">Specializes in digital marketing  </div>
     </div>
   </div>
 </div>
</div>

<!--Featured Block-->
<div class="featured-block col-md-4 col-sm-6 col-xs-12">
 <div class="inner-box">
  <div class="upper-box">
   <div class="text"><?=Digital_Marketing_info ?></div>
   <a class="read-more" href="crypto_marketing_services.php"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
 </div>
 <div class="lower-box">
   <div class="lower-inner">
    <div class="icon-box">
     <span class="icon"><img src="images/svg/ico_digitalm.svg" alt="" /></span>
   </div>
   <h3><a href="crypto_marketing_services.php" class="textlinks" style="color:#ff4800;"><?=Digital_Marketing ?></a></h3>
   <div class="title">Businesses leverage</div>
 </div>
</div>
</div>
</div>

<!--Featured Block-->
<div class="featured-block col-md-4 col-sm-6 col-xs-12">
 <div class="inner-box">
  <div class="upper-box">
   <div class="text"><?=Blockchain_info ?></div>
   <a class="read-more" href="crypto_development_services.php"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
 </div>
 <div class="lower-box">
   <div class="lower-inner">
    <div class="icon-box">
     <span class="icon"><img src="images/svg/ico_blockchaindev.svg" alt="" /></span>
   </div>
   <h3><a href="crypto_development_services.php" class="textlinks" style="color:#ff4800;"><?=Blockchain ?></a></h3>
   <div class="title">Decentralized, distributed</div>
 </div>
</div>
</div>
</div>

</div>
</div>
</section>
<!--End Featured Section-->

<!--Featured Section Two-->
<section class="featured-section-two" style="background-image:url(images/background/pattern-2.png)">
 <div class="auto-container">
   <!--Sec Title-->
   <div class="sec-title centered">
     <h2><?= Services ?></h2>
   </div>
   <div class="inner-container">
     <div class="clearfix">

       <!--Featured Block Two-->
       <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
         <div class="inner-box">
           <div class="icon-box">
             <span class="icon"><img src="images/svg/ico_development.svg" width="80px" /></span>
           </div>
           <h3><a href="crypto_development_services.php"><?= Development ?></a></h3>
           <div class="text">
               <?=All_types ?>
           </div>
         </div>
       </div>

       <!--Featured Block Two-->
       <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="icon-box">
            <span class="icon"><img src="images/svg/ico_legal.svg" width="70px" /></span>

          </div>
          <h3><a href="legal_assistance.php"><?= LegalAssistance ?></a></h3>
          <div class="text"><?=Our_goal ?></div>

        </div>
      </div>

      <!--Featured Block Two-->
      <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
       <div class="inner-box">
         <div class="icon-box">
           <span class="icon"><img src="images/svg/ico_marketing.svg" width="60px" /></span>
         </div>
         <h3><a href="crypto_marketing_services.php"><?=Marketing ?></a></h3>
         <div class="text"><?= Marketing_services ?></div>
       </div>
     </div>

     <!--Featured Block Two-->
     <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
      <div class="inner-box">
        <div class="icon-box">
          <span class="icon"><img src="images/svg/ico_advisor.svg" width="74px" /></span>
        </div>
        <h3><a href="crypto_advisor.php"><?= Crypto_Advisor_Placement ?></a></h3>
        <div class="text"><?=Cryptocurrency_adisors ?></div>
      </div>
    </div>

    <!--Featured Block Two-->
    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
      <div class="inner-box">
        <div class="icon-box">
          <span class="icon"><img src="images/svg/ico_launching.svg" width="74px" /></span>

        </div>
        <h3><a href="ico_launching_services.php"><?=ICO_STO_Launching ?></a></h3>
        <div class="text"><?=ico_sto_h ?></div>
      </div>
    </div>

    <!--Featured Block Two-->
    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
      <div class="inner-box">
        <div class="icon-box">
          <span class="icon"><img src="images/svg/ico_listing.svg" width="74px" /></span>

        </div>
        <h3><a href="exchanges_listing.php"><?=Exchanges ?></a></h3>
        <div class="text"><?= exchange_listing_h ?></div>
      </div>
    </div>

</div>
</div>
</div>
</section>
<!--End Featured Section Two-->

<!--Testimonial Section-->
<section class="testimonial-section" style="background-image:url(images/background/why-working-with-us.jpg)">
  <div class="auto-container">
   <div class="row clearfix">

    <!--Title Column-->
    <div class="title-column col-md-6 col-sm-12 col-xs-12">
     <div class="inner-column">
      <h2 style="font-size: 50pt;"><?=whyUs ?></h2>
    </div>
  </div>
  <!--Testimonial Column-->
  <div class="testimonial-column col-md-6 col-sm-12 col-xs-12">
   <div class="testimonial-carousel owl-carousel owl-theme">
    <!--Testimonial Block-->
    <div class="testimonial-block">
     <div class="inner-box">
      <!--Title Column-->
      <div class="title-column col-md-12 col-sm-12 col-xs-12">
       <div class="inner-column">
        <ul>
         <li>
          <span style="font-size: 20pt; color:#FC4903;"><?=Best_Marketing ?></span>
          <ul>
           <li>
            <span style="font-size: 11pt; color: #DDDDDD"><?=Best_Marketing_info ?></span>
         </li>
       </ul>
       <p>&nbsp;</p>

     </li>

     <li><span style="font-size: 20pt;color:#FC4903"><?=Latest_Tech ?></span>
      <ul>
       <li><span style="font-size: 11pt; color: #DDDDDD"><?=Latest_Tech_info ?></span>
     </li>
   </ul><p>&nbsp;</p></li>
   <li><span style="font-size: 20pt;color:#FC4903"><?=Scalable_Solutions ?></span>
    <ul>
     <span style="font-size: 11pt; color: #DDDDDD"><?=Scalable_Solutions_info ?></span>
  </ul></li>
</ul>

</div>
</div>


</div>
</div>



</div>
</div>

</div>
</div>
</section>
<!--End Testimonial Section-->


<!--Services Section-->
<section class="services-section">
 <div class="auto-container">
  <!--Sec Title-->
  <div class="sec-title centered">
      <h2><?=OurNews ?></h2>
      <div class="text"><?=OurNews_info ?></div>
 </div>

 <div class="three-item-carousel owl-carousel owl-theme">
  <!--Services Block-->
  <div class="services-block">
    <div class="inner-box">
     <div class="icon-box">
      <span class="icon"><img src="images/svg/ico_advisor.svg"/ style="height: 100px;"></span>

    </div>
        <h3><a href="services.php"><?=Crypto_Advisor_Placement ?></a></h3>
        <div class="text"><?=Cryptocurrency_adisors ?></div>
        <a href="services.php" class="read-more"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
  </div>
</div>

<!--Services Block-->
<div class="services-block">
 <div class="inner-box">
  <div class="icon-box">
   <span class="icon"><img src="images/svg/ico_development.svg"  style="height: 100px;"/></span>
 </div>
 <h3><a href="services.php"><?= Development ?></a></h3>
 <div class="text"><?= All_types ?></div>
<a href="services.php" class="read-more"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
</div>
</div>

<!--Services Block-->
<div class="services-block">
 <div class="inner-box">
  <div class="icon-box">
   <span class="icon"><img src="images/svg/ico_advisor.svg" style="height: 100px;"/></span>
 </div>
 <h3><a href="services.php"><?= LegalAssistance ?></a></h3>
 <div class="text"><?= Our_goal ?></div>
 <a href="services.php" class="read-more"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
</div>
</div>

<!--Services Block-->
<div class="services-block">
 <div class="inner-box">
  <div class="icon-box">
   <span class="icon"><img src="images/svg/ico_marketing.svg" style="height: 100px;"/></span>


 </div>
 <h3><a href="services.php"><?= Marketing ?></a></h3>
 <div class="text"><?= Marketing_services ?></div>
 <a href="services.php" class="read-more"><span class="icon flaticon-right-arrow-1"></span> <?=ReadMore ?></a>
</div>
</div>
</div>
</div>
</section>
<!--End Services Section-->


<!--Marketing Section-->
<section class="marketing-section">
  <div class="auto-container">
   <div class="row clearfix">
    <!--Image Column-->
    <div class="image-column col-md-6 col-sm-12 col-xs-12">
     <div class="image">
      <img class="img-responsive faq-img" src="images/svg/FAQ.svg"/>
    </div>
  </div>
  <!--Content Column-->
  <div class="content-column col-md-6 col-sm-12 col-xs-12">
   <div class="inner-column">
    <h2>FAQ</h2>
    <div class="single-item-carousel owl-carousel owl-theme">

     <!--Market Content-->
     <div class="market-content">
      <div class="content-inner">
       <div class="content-number">1.</div>
       <h3><?=digital_marketing ?></h3>
       <div class="text" style="color: white;"><?=digital_marketing_info ?></div>
    </div>
  </div>

  <!--Market Content-->
  <div class="market-content">
   <div class="content-inner">
    <div class="content-number">2.</div>
    <h3><?=business_benefit ?></h3>
    <div class="text" style="color: white;"><?=business_benefit_info ?></div>
 </div>
</div>

<!--Market Content-->
<div class="market-content">
 <div class="content-inner">
  <div class="content-number">3.</div>
  <h3><?=content_marketing ?></h3>
  <div class="text" style="color: white;"><?=content_marketing_info ?></div>
</div>
</div>

<!--Market Content-->
<div class="market-content">
 <div class="content-inner">
  <div class="content-number">4.</div>
  <h3><?=need_blog ?></h3>
  <div class="text" style="color: white;"><?=need_blog_info ?></div>
</div>
</div>

<!--Market Content-->
<div class="market-content">
 <div class="content-inner">
  <div class="content-number">5.</div>
  <h3><?=content_create ?></h3>
  <div class="text" style="color: white;"><?=content_create_info ?></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End Marketing Section-->

<!--Map Section-->
<div id="map-home">
 <section class="map-section">
  <!--Map Outer-->
  <div class="map-outer">
   <!--Map Canvas-->
   <div class="map-canvas"
   data-zoom="12"
   data-lat="25.150122999999997"
   data-lng="55.2430661"
   data-type="roadmap"
   data-hue="#ffc400"
   data-title="Tokenncoin"
   data-icon-path=""
   data-content="One By Omniyat, Dubai<br><a href='mailto:info@gmail.com'>info@gmail.com</a>">
 </div>
</div>
</section>
</div>
<!--Map Section-->

<!--Partners Section-->
<section class="partners-section">
 <div class="auto-container">
  <div class="row clearfix">

   <!--Title Column-->
   <div class="title-column col-md-4 col-sm-12 col-xs-12">
     <div class="inner-column">
      <h2><?=Blog ?></h2>
      <div class="text">Must explain to you how all this mistaken idea of denouncing pleasure and praising pain was  complete account of the system.</div>
      <a href="blog.php" class="theme-btn btn-style-one"><?=JoinUs ?></a>
    </div>
  </div>

  <!--Partners Column-->
  <div class="partners-column col-md-8 col-sm-12 col-xs-12">
    <div class="row clearfix">
     <!--Column-->
     <div class="column col-md-6 col-sm-6 col-xs-12">
      <div class="partner-block">
        <div class="inner-box">
         <div class="client-img">
          <img src="images/clients/1.png" alt="" />
        </div>
        <div class="text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</div>
        <a class="read-more" href="blog.php"><span class="icon flaticon-right-arrow-1"></span> <?=Visitwebsite ?></a>
      </div>
    </div>
  </div>
  <!--Column-->
  <div class="column col-md-6 col-sm-6 col-xs-12">
    <div class="partner-block">
      <div class="inner-box">
       <div class="client-img">
        <img src="images/clients/2.png" alt="" />
      </div>
      <div class="text">The master-builder of human happiness one avoids pleasure itself, because it is pleasure, but because those who do not know pursue.</div>
      <a class="read-more" href="blog.php"><span class="icon flaticon-right-arrow-1"></span> <?=Visitwebsite ?></a>
    </div>
  </div>
</div>
</div>
</div>

</div>
</div>
</section>
<!--End Partners Section-->

<!--Featured Section Two-->
<section  class="marketing-section" style="">
  <div class="auto-container">
    <div class="clearfix">
     <!--Column-->
     <div class="column col-md-12 col-sm-12 col-xs-12 text-center" >
      <p class="marketing-section-p" style="margin: 20px; color: white;"><?=Subscribe_info ?></p>
      <form id="search-form" class="form-inline" role="form" method="post" action="" target="_blank">
       <div class="input-group">
           <input type="email" name="email" class="form-control search-form" placeholder="account@gmail.com" required>
        <span class="input-group-btn">
            <button type="submit" class="btn btn-primary search-btn" data-target="#search-form" name="q"><?=Subscribe ?>
        </button></span>
      </div>
    </form>
  </div>
</div>
</div>
</section>
<!--End Featured Section Two-->

<?php include "footer.php"; ?>
