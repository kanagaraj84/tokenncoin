<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title><?= ICO_STO_Launching ?></title>
    <style>
        .inner-box {
            height: 350px;
        }
    </style>
    <?php include "topheader.php"; ?>
 	
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/ICO-launching.png);">
    </section>
    <!--End Page Title-->


    <!--We Are Section-->
    <section class="we-are-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title centered">
                            <p class="icon"><img src="images/svg/ico_launch.svg" width="200" alt="" /></p>
                            <h2><?= ICO_STO_Launching?></h2>
                            <div class="text" style="text-align: left;"><?=ICO_STO_info ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End We Are Section-->

    <!--Featured Section Two-->
    <section class="featured-section-two" style="background-image:url(images/background/pattern-2.png)">
        <div class="auto-container">
            <div class="inner-container">
                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/review.svg" width="110" alt="" /></span>
                            </div>
                            <h3><?= Review?></h3>
                            <div class="text"><?= We_make ?></div>

                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/agency.svg" width="100" alt="" /></span>
                            </div>
                            <h3><?= Agency_Sign_Up ?></h3>
                            <div class="text"><?= We_are ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/token.svg" width="97" alt="" /></span>
                            </div>
                            <h3><?= Token_Sales ?></h3>
                            <div class="text"><?= We_work_to_increase ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include "footer.php"; ?>