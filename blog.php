<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= Blog ?></title>
    <?php include "topheader.php"; ?>

    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/blog.jpg);">
        <!--End Page Info-->
    </section>
    <!--End Page Title-->
    
    <!--Blog Page Section-->
    <section class="blog-page-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-1.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">February 26th, 2018</div>
                            	<h3><a href="blog-detail.php">Easy steps to move from fear to confidence.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-2.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">January 14th, 2018</div>
                            	<h3><a href="blog-detail.php">How to get more done and have more fun.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-3.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">December 06th, 2017</div>
                            	<h3><a href="blog-detail.php">Financial growth in your down economy.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-4.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">December 06th, 2017</div>
                            	<h3><a href="blog-detail.php">Financial growth in your down economy.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-5.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">February 26th, 2018</div>
                            	<h3><a href="blog-detail.php">Easy steps to move from fear to confidence.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-6.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">January 14th, 2018</div>
                            	<h3><a href="blog-detail.php">How to get more done and have more fun.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-7.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">January 14th, 2018</div>
                            	<h3><a href="blog-detail.php">How to get more done and have more fun.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-8.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">December 06th, 2017</div>
                            	<h3><a href="blog-detail.php">Financial growth in your down economy.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="blog-detail.php"><img src="images/resource/news-9.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                        	<div class="upper">
                        		<div class="post-date">February 26th, 2018</div>
                            	<h3><a href="blog-detail.php">Easy steps to move from fear to confidence.</a></h3>
                            </div>
                            <div class="more">
                            	<a href="blog-detail.php" class="read-more"><span class="fa fa-chevron-circle-right"></span> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Blog Page Section-->

<?php include "footer.php"; ?>