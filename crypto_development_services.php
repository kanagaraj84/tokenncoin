<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?= Development ?></title>
    <style>
        .inner-box {
            height: 320px;
        }
    </style>
    <?php include "topheader.php"; ?>
 	
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/development.jpg);">
    </section>
    <!--End Page Title-->
 

    <!--We Are Section-->
    <section class="we-are-section">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Carousel Column-->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title centered">
                            <p class="icon"><img src="images/svg/ico_development.svg" width="200px" alt="" /></p><br />
                            <h2><?= Development ?></h2>
                            <div class="text" style="text-align: left;"><?=Development_info ?></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End We Are Section-->

    <!--Featured Section Two-->
    <section class="featured-section-two" style="background-image:url(images/background/pattern-2.png)">
        <div class="auto-container">
            <div class="inner-container">
                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <p class="icon"><img src="images/svg/custom_blockchain.svg" width="70px;" alt="" /></p>
                                <h3><?= Custom_Blockchain_Solution ?></h3>
                                <div class="text"><?=We_develop_blockchain ?></div>
                            </div>

                        </div>
                    </div>

                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/sto_launching.svg" width="86px;"  /></span>
                            </div>
                            <h3><a href="ico_launching_services.php"><?= ICO_STO_Launching ?></a></h3>
                            <div class="text"><?=Starting_your ?></div>
                        </div>
                    </div>

                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/smart_contract.svg" width="75px; alt="" /></span>
                            </div>
                            <h3><?= Smart_Contract_Service ?></h3>
                            <div class="text"><?= We_develop ?></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/webdev.svg" width="70px" alt="" /></span>
                            </div>
                            <h3><?= Web_Development ?></h3>
                            <div class="text"><?= An_engaging ?></div>
                        </div>
                    </div>

                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/mobileapp.svg" width="62px;" alt="" /></span>
                            </div>
                            <h3><?= Mobile_App_Development ?></h3>
                            <div class="text"><?= Mobile_device ?></div>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
    <!--End Featured Section Two-->

<?php include "footer.php"; ?>