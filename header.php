<?php
if(isset($language) && $language!=''){
    if($language=='lang=en'){
        $lname = 'English';
    } else if($language=='lang=kr'){
        $lname = 'Korean';
    } else if($language=='lang=cn'){
        $lname = 'Chinese';
    } else if($language=='lang=ru'){
        $lname = 'Russian';
    }
    $language = '?'.$language;
} else {
    $language = ''; $lname = 'English';
}?>
<!-- Header Top -->
<div class="header-top">
    <div class="auto-container">
        <div class="clearfix">

            <!--Top Left-->
            <div class="top-right">
                <ul class="links clearfix">
                    <!--Language-->
                    <li class="language dropdown">
                        <a class="btn btn-default dropdown-toggle " id="dropdownMenu2" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true" href="#"  style="text-decoration: none;">
                        <span class="flag-img"><img src="images/icons/flag.jpg" alt="" /></span><?=$lname ?> &nbsp;<span class="fa fa-angle-down"></span></a>
                        <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                            <li><a href="?lang=en" id="toplink">English</a></li>
                            <li><a href="?lang=kr" id="toplink">Korean</a></li>
                            <li><a href="?lang=cn" id="toplink">Chinese</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" id="toplink"><span class="icon fa fa-facebook" style="color: white; padding-top: 3px;"></span></a>
                        <a href="#" id="toplink"><span class="icon fa fa-twitter" style="color: white; padding-top: 3px;"></span></a>
                        <a href="#" id="toplink"><span class="icon fa fa-instagram" style="color: white; padding-top: 3px;"></span></a>
                        <a href="#" id="toplink"><span class="icon fa fa-youtube" style="color: white; padding-top: 3px;"></span></a>
                    </li>

                </ul>
            </div>
        </div>

    </div>
</div>
<!-- Header Top End -->

<!-- Main Box -->
<div class="main-box">
    <div class="auto-container">
        <div class="outer-container clearfix">
            <!--Logo Box-->
            <div class="logo-box">
                <div class="logo"><a href="index.php<?=$language ?>"><img class="img-responsive" src="images/svg/tokenncoin_logo.svg" width="250px;"></a></div>
            </div>

            <!--Nav Outer-->
            <div class="nav-outer clearfix">

                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li><a href="index.php<?=$language ?>"><?= HOME_PAGE ?></a></li>
                            <li><a href="crypto_marketing_company.php<?=$language ?>"><?= Company ?></a></li>
                            <li class="dropdown"><a href="#"><?= Services ?></a>
                                <ul>
                                    <li><a href="crypto_development_services.php<?=$language ?>"><?= Development ?></a></li>
                                    <li><a href="legal_assistance.php<?=$language ?>"><?= LegalAssistance ?></a></li>
                                    <li><a href="crypto_marketing_services.php<?=$language ?>"><?= Marketing ?></a></li>
                                    <li><a href="crypto_advisor.php<?=$language ?>"><?= Crypto_Advisor ?></a></li>
                                    <li><a href="market_making.php<?=$language ?>"><?= Market_Making ?></a></li>
                                </ul>
                            </li>
                            <li><a href="exchanges_listing.php<?=$language ?>"><?= Exchanges ?></a></li>
                            <li><a href="ico_launching_services.php<?=$language ?>"><?= ICO_STO_Launching ?></a></li>
                            <li><a href="blog.php<?=$language ?>"><?= Blog ?></a></li>
                            <li><a href="contact.php<?=$language ?>"><span class="icon"></span><?=ContactUs ?></a></li>
                        </ul>
                    </div>

                </nav>
                <!-- Main Menu End-->



            </div>
            <!--Nav Outer End-->

        </div>
    </div>
</div>

<!--Sticky Header-->
<div class="sticky-header">
    <div class="auto-container">

        <div class="outer-container clearfix">
            <!--Logo Box-->
            <div class="logo-box pull-left">
                <div class="logo"><a href="index.php<?=$language ?>"><img src="images/logo-small.png" alt=""></a></div>
            </div>

            <!--Nav Outer-->
            <div class="nav-outer clearfix">
                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li><a href="index.php<?=$language ?>"><?= HOME_PAGE ?></a></li>
                            <li><a href="crypto_marketing_company.php<?=$language ?>"><?= Company ?></a></li>
                            <li class="dropdown"><a href="#"><?= Services ?></a>
                                <ul>
                                    <li><a href="crypto_development_services.php<?=$language ?>"><?= Development ?></a></li>
                                    <li><a href="legal_assistance.php<?=$language ?>"><?= LegalAssistance ?></a></li>
                                    <li><a href="crypto_marketing_services.php<?=$language ?>"><?= Marketing ?></a></li>
                                    <li><a href="crypto_advisor.php<?=$language ?>"><?= Crypto_Advisor ?></a></li>
                                    <li><a href="market_making.php<?=$language ?>"><?= Market_Making ?></a></li>
                                </ul>
                            </li>
                            <li><a href="exchanges_listing.php<?=$language ?>"><?= Exchanges ?></a></li>
                            <li><a href="ico_launching_services.php<?=$language ?>"><?= ICO_STO_Launching ?></a></li>
                            <li><a href="blog.php<?=$language ?>"><?= Blog ?></a></li>
                            <li><a href="contact.php<?=$language ?>"><span class="icon"></span><?=ContactUs ?></a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Main Menu End-->

            </div>
            <!--Nav Outer End-->

        </div>

    </div>
</div>
    <!--End Sticky Header-->