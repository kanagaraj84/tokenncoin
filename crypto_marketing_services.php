<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title><?= Marketing ?></title>
    <style>
        .inner-box {
            height: 318px;
        }
    </style>
    <?php include "topheader.php"; ?>
 	
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/markeitng.jpg);">
    </section>
    <!--End Page Title-->


    <!--Featured Section Two-->
    <section class="featured-section-two" style="background-image:url(images/background/pattern-2.png)">
        <div class="auto-container">
            <!--We Are Section-->
            <section class="we-are-section">
                <div class="auto-container">
                    <div class="row clearfix">
                        <!--Content Column-->
                        <div class="content-column col-md-12 col-sm-12 col-xs-12">
                            <div class="inner-column">
                                <div class="sec-title centered">
                                    <p class="icon"><img src="images/svg/ico_marketing.svg" width="200" alt="" /></p><br />
                                    <<h2><?= Marketing ?></h2>
                                    <div class="text" style="text-align: left;"><?=Marketing_info ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End We Are Section-->

            <div class="inner-container">
                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/seo.svg" width="70" alt="" /></span>
                            </div>
                            <h3><?= SEO ?></h3>
                            <div class="text"><?= Search_Engine ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/smm.svg" width="80" alt="" /></span>
                            </div>
                            <h3><?= SMM ?></h3>
                            <div class="text"><?= Social_Media ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/com.svg" width="82" alt="" /></span>
                            </div>
                            <h3><?= Community_management ?></h3>
                            <div class="text"><?= It_is_very ?></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/icolist.svg" width="76" alt="" /></span>
                            </div>
                            <h3><?= ICO_Listing ?></h3>
                            <div class="text"><?= Listing_your ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/content.svg" width="70" alt="" /></span>
                            </div>
                            <h3><?= Content_Marketing ?></h3>
                            <div class="text"><?= Making_content ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/padd.svg" width="83" alt="" /></span>
                            </div>
                            <h3><?= Paid_Advertising ?></h3>
                            <div class="text"><?= We_provide_p ?></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/nadd.svg" width="74" alt="" /></span>
                            </div>
                            <h3><?= Native_Advertising ?></h3>
                            <div class="text"><?= We_provide ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/prmarket.svg" width="70" alt="" /></span>
                            </div>
                            <h3><?= PR_Marketing ?></h3>
                            <div class="text"><?= Press_releases ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/infmarket.svg" width="88" alt="" /></span>
                            </div>
                            <h3><?= Influencer_Marketing_l ?></h3>
                            <div class="text"><?= Influencer_Marketing ?></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/airdrop.svg" width="70" alt="" /></span>
                            </div>
                            <h3><?= Airdrop_Campaign ?></h3>
                            <div class="text"><?= Airdrop_campaigns ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/emarket.svg" width="68" alt="" /></span>
                            </div>
                            <h3><?= Email_Marketing ?></h3>
                            <div class="text"><?= Marketing_through ?></div>
                        </div>
                    </div>
                    <!--Featured Block Two-->
                    <div class="featured-block-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon"><img src="images/svg/vmarket.svg" width="87" alt="" /></span>
                            </div>
                            <h3><?= Video_Marketing ?></h3>
                            <div class="text"><?= Connecting_with ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include "footer.php"; ?>