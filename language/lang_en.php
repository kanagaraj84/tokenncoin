<?php
define("connect_with_crypto", "CONNECT WITH CRYPTO");
define("title", "Token'n Coin ");
define("HOME_PAGE", "HOME");
define("Company", "Company");
define("Tokenncoin", "<p>Tokenncoin is the best cryptocurrency consultancy firm in the Middle East. 
With the goal to help cryptocurrency projects succeed, Tokenncoin works with the best blockchain developers, marketing 
experts, business development and recruitment officers to help each and every client meet their cryptocurrency project’s 
goals. </p> <p>We believe that blockchain technology is an innovative industry. This is why our business is to support blockchain projects. 
By maximizing the potential of each blockchain project, we are supporting the blockchain industry directly. 
We have the capability and potential to explore and share the value that our clients are offering to their 
respective communities.</p> <p>It is our objective to connect these blockchain projects to a community that supports and believes in 
the value and contribution that they are offering to the world of blockchain. This is the value that our 
company is offering to our potential clients who are making their own blockchain projects. </p>");
define("Exchanges", "Exchanges Listing");
define("Listing_on", "Listing on exchanges is a very big milestone for your cryptocurrency project. Listing is never easy because of the difference in requirements from various exchanges. Listing strategies are different for each project. Some projects choose to list on an exclusive exchange, and some projects opt for multiple exchanges listing. Tokenncoin aims to help clients execute their listing strategy and collaborate with our partner exchanges to make listing more successful for clients. ");
define("Services", "Services");
define("Marketing", "Marketing");
define("Marketing_services", "Services for cryptocurrency projects ranging from planning, branding, design, advertising, search engine optimization, social media marketing, email marketing and content marketing.");
define("Development","Development");
define("All_types", "All types of development for our clients ranging from blockchain development, token and coin creation, website development, Android and iOS development and more IT solutions. ");
define("LegalAssistance", "Legal Advice");
define("Our_goal", "Our goal to ensure that our clients’ cryptocurrency projects abide by regulatory standards. Crypto projects need to follow the guidelines within the jurisdiction of governing bodies.");
define("Crypto_Advisor_Placement", "Crypto Advisor");
define("Crypto_Advisor", "Crypto Advisor ");
define("Cryptocurrency_adisors", "Cryptocurrency advisors are very crucial to the success of a cryptocurrency project. Every project needs to have a reliable and experienced cryptocurrency adviser to help them with their needs.");
define("Market_Making", "Market Making");
define("We_build", "We build markets because we understand the value our customers can enjoy from having an efficient market and can tackle the complexities around it. ");
define("SEO", "SEO");
define("Search_Engine", "Search Engine Optimization is the practice of organically optimizing your website to gain more visibility in organic search engine results through the usage of keywords. ");
define("SMM", "SMM");
define("Social_Media", "Social Media Marketing is the practice of using social media to gain awareness of the brand. We employ various social media marketing techniques with the help of the best social media marketers in the crypto industry. ");
define("Community_management", "Community  Management");
define("It_is_very", "It is very important to have a happy growing community for any cryptocurrency project. All businesses find success when there is a support from a community. ");
define("ICO_Listing", "ICO Listing and Ratings");
define("Listing_your", "Listing your ICO and getting good reviews is a very important marketing method that could raise the reputation of your brand. We make sure that our clients are listed in the best ICO websites and get the best reviews for their project. ");
define("Content_Marketing", "Content Marketing");
define("Making_content", "Making content that is very helpful for your target audience is of utmost importance. We have experts in content marketing that will help create great content for our clients. ");
define("Paid_Advertising", "Paid Advertising");
define("We_provide_p", "We provide paid advertising solutions for our clients. Paid advertising can help you reach many of your targeted audience to be aware of your project. ");
define("Native_Advertising", "Native Advertising");
define("We_provide", "We provide solutions to publish native ads on various famous cryptocurrency websites. Native ads has the potential to reach a wide audience as well depending on site traffic. ");
define("PR_Marketing", "PR Marketing");
define("Press_releases", "Press releases are an important part of cryptocurrency marketing. Because people value information, we provide press release writing and publication services to our clients.  ");
define("Influencer_Marketing_l", "Influencer Marketing ");
define("Influencer_Marketing", "Influencer marketing includes building business relationships with influencers with many active followers to advertise our clients’ project. ");
define("Bounty_Program", "Bounty Program Consulting");
define("A_great_way", "A great way to engage with your audience is through a bounty program. This will boost people’s awareness of your brand and attract supporters. ");
define("Airdrop_Campaign", "Airdrop Campaign Consulting");
define("Airdrop_campaigns", "Airdrop campaigns are also an effective marketing strategy. By giving out free coins to people who would be participants, a project can increase the number of their followers. ");
define("Email_Marketing", "Email Marketing");
define("Marketing_through", "Marketing through email is one of the most effective strategies in the new media. We provide growth strategies for email lists and mass email marketing services");
define("Video_Marketing", "Video Marketing");
define("Connecting_with", "Connecting with your audience is best done through videos. Using video as a medium is a good investment in your marketing efforts. ");
define("Custom_Blockchain_Solution", "Custom Blockchain Solution ");
define("We_develop_blockchain", "We develop blockchain solutions with a very wide range of services based on every client’s needs. We have solutions for any type of requirement depending on the project.");
define("Smart_Contract_Service", "Smart Contract Service");
define("We_develop", "We develop smart contract solutions based on the Ethereum. This includes making ERC20 tokens or any form of smart contract suitable for our clients’ requirements.  ");
define("Web_Development", "Web Development");
define("An_engaging", "An engaging and responsive website is a big must for any blockchain project. If you need a website, we can develop one from scratch or modify your existing website to serve you better. ");
define("Mobile_App_Development", "Mobile App Development");
define("Mobile_device", "Mobile device usage is continually growing. It is a great way to engage with your audience as well. It is a necessary requirement for any cryptocurrency project. ");
define("ICO_STO_Launching", "ICO/STO Launching");
define("Starting_your", "Starting your ICO and STO will make your project face many difficulties. Launch your ICO and STO with us! We have teams with people who are very exceptional working dedicatedly for making your ICO or STO launching very successful. ");
define("ommunity_management", "Community management");
define("Review", "Review");
define("We_make", "We make sure to get the most appropriate reviewers for your project based on the quality that the project brings to the world. ");
define("Agency_Sign_Up", "Agency Sign Up");
define("We_are", "We are connecting you with the best agencies that can be a huge contribution to your ICO or STO.");
define("Token_Sales", "Token Sales");
define("We_work_to_increase", "We work to increase visibility and handle sales for your ICO or STO. Token sales with greatly affect the quality of your project because it is where you can get the initial investments to make your project more successful. ");
define("Blog", "Blog");
define("Find_Tokenncoin", "Find Tokenncoin updates, news and promotions in this section of the Tokenncoin website.");
define("ico_sto_h", "We are offering ICO and STO Launching services. Our services can help you be more successful in your ICO or STO. Our services include STO and ICO Review, Agency Sign Up and Token Sales.");
define("exchange_listing_h", "Listing on Exchanges is the biggest goal for a cryptocurrency project. We can make this challenge easier for our customers with our existing partnerships with top exchanges.");

//newly added constants
define("HomeSlide1_c1", "<span>Advanced</span> marketing solutions for blockchain companies");
define("HomeSlide1_c2", "The best marketing strategies that aligns with the goals of your business are within your grasp.");
define("ReadMore", "Read More");
define("HomeSlide2_c1", "<span>Work</span>toward the success of your cryptocurrency project");
define("HomeSlide2_c2", "We Offering Digital Marketing Solutions that can Tailored <br> to Suit any Budget and Business.");
define("HomeSlide3_c1", "<span>Wide</span>range of services all in one place");
define("HomeSlide3_c2", "We can help you with any type of blockchain related concern especially when it comes to 
tokens and coins.");

define("Digital_Marketing", "Digital Marketing");
define("Digital_Marketing_info", "Digital marketing encompasses all marketing efforts that use an electronic device or 
the internet.");
define("company_info", "Our company specializes in digital marketing and development services for clients in the 
cryptocurrency industry.");
define("Blockchain", "Blockchain Development");
define("Blockchain_info", "We developed decentralized, distributed and public digital ledgers that are used to record 
transactions across many computers.");

define("whyUs", "Why work<br /> with us?");
define("Best_Marketing", "Best Marketing Solutions");
define("Best_Marketing_info", "We have teams of experts with years of experience<br /> in marketing for the cryptocurrency industry.");
define("Latest_Tech", "Latest Technologies in Blockchain");
define("Latest_Tech_info", "Our blockchain specialists can apply the most recent<br /> technologies for developing your project.");
define("Scalable_Solutions", "Scalable Solutions");
define("Scalable_Solutions_info", "Our services include a wide variety and can easily<br /> adapt to our client needs.");

define("OurNews", "OUR NEWS");
define("OurNews_info", "We help you generate high-quality online sales leads by implementing highly structured, 
persuasive Internet marketing campaigns. ");

define("FAQ", "FAQ");
define("digital_marketing", "What is digital marketing?");
define("digital_marketing_info", "Digital marketing involves marketing to people using Internet-connected electronic 
devices, namely computers, smartphones and tablets. Digital marketing focuses on channels such as search engines, 
social media, email, websites and apps to connect with prospects and customers.");
define("business_benefit", "Would my business benefit from digital marketing?");
define("business_benefit_info", "Definitely. Though companies in many business categories continue to approach digital 
marketing with skepticism, avoiding digital marketing denies your business access to the media the majority of 
consumers turn to first and at all hours of the day.");
define("content_marketing", "What is content marketing?");
define("content_marketing_info", "Content marketing is the creation and distribution of educational and/or entertaining 
information assets for the purpose of generating brand awareness, traffic, leads and sales. Marketing content is 
generally free and does not explicitly promote your brand as an ad would.");
define("need_blog", "Do I need a blog?");
define("need_blog_info", "Email, social media, search and other elements of your digital marketing mix will depend on 
delivering useful content. Though video outlets, podcasts, and media galleries present other options, a blog is by far 
the most used tactic for distributing marketing content on a channel over which you have complete control.");
define("content_create", "What content should we create?");
define("content_create_info", "There can be no all-encompassing answer to this question except to say you should create 
content buyers will find relevant and useful. Leading candidates include blog posts, articles, ebooks, infographics, 
microsites, videos, courses, case studies, newsletters, visuals and various forms of interactive content.");

define("Subscribe", "Subscribe");
define("Subscribe_info", "Subscribe to our newsletter to receive exclusive offers and the latest news on our services.");

define("JoinUs", "Join with Us");
define("Visitwebsite", "Visit Website");

define("ContactUs", "Contact Us");
define("Useful_Links", "USEFUL LINKS");
define("Corporate_Office", "Corporate Office");
define("Addresslabel", "Address: ");
define("Address", "DUBAI, Business Bay, One by Omniyat Tower, office -1104");
define("Telephonelabel", "Telephone: ");
define("Telephone", "+971 4 874 8165");
define("Find_Map", "Find Us On Map");

define("Development_info", "<p>We are providing end-to-end cryptocurrency solutions to the clients. 
We are also creating scalable systems that support fast transactions. We created cryptocurrency systems for enterprises 
and startups based on blockchain technology.</p><br />
<p>Our services include cryptocurrency coins and token development, wallet development and smart contract development. 
We also provide custom blockchain solutions that can be flexible according to client needs.</p><br />
<p>We develop blockchain solutions with a very wide range of services based on every client’s needs. We have solutions for 
any type of requirement depending on the project. This includes coin or token creation and any form of blockchain 
development. This also includes development for wallets. </p>");

define("legal_info", "<p>Initial Coin Offerings or ICOs have increased in number. Aside from the Howey Test, there are various multifaceted concerns to consider when performing an ICO.
 With our wide-ranging knowledge in the industry of blockchain, we are capable of tailoring a very comprehensive strategy to ensure a successful
ICO launch / Token Sale from a legal standpoint.</p> <br />
<p>Our legal services include:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; Corporate Structure<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Jurisdiction Selection<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Regulatory Compliance<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; KYC/AML Framework<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Offering Memorandums and Purchase Agreements (including SAFTs)<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Legal Opinion on Utilities and Securities Token<br>
</p><br />
<p>Cryptocurrency trading volumes have increased worldwide. Because of these, the number
    of cryptocurrency exchange disputes also naturally gets bigger.
    These disputes are more common among cryptocurrency exchanges that strictly handle
    alternative coins (alt-coins) and are not accepting fiat currencies. </p><br />
<p>Most common disputes include: </p>
<p>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Cryptocurrency Withdrawal Issues<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Cryptocurrency Deposit Issues<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Account Ban<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Asset Freeze<br>
</p>");

define("Marketing_info", "<p>Tokenncoin delivers world-class marketing solutions for blockchain companies. Reaching both
 crypto and traditional audiences, Tokenncoin executes proven strategy to drive mass awareness of your project. With 
 awareness comes adoption and conversation, generating goal-oriented token sales.</p>
<p> Building up your platform's user base takes center stage when Tokenncoin applies our sophisticated marketing 
strategies across multifaceted digital channels to create strong month-over-month growth.</p>
<p>Tokenncoin team members are experts in marketing and are experienced in the leading industry projects. We can 
easily connect our clients’ projects to both the cryptocurrency community and the business to business sector 
working towards the development of blockchain.</p>");

define("Crypto_Advisor_info", "<p>Tokenncoin is rewriting the way we organize, and will continue to improve the lives 
of many clients and their target audience who are enthusiasts and participants in the cryptocurrency industry.</p>
<p>Emerging economic paradigms are rapidly transforming the way people trade. Innovative figures of value are already 
being developed. From the utmost beginning to the launching, part of our focus is on connecting advisors to the top 
cryptocurrency projects and creating meaningful impact.</p>
<p>We work with projects that have very high social value and have the capability to change people’s lives. If you are 
looking to top-notch advisors for your cryptocurrency project, we are a leading global agency that can connect you with 
the right people.</p> <p>We are offering different type of advisors for your crypto project:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; ICO advisors<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; STO advisors<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Legal advisors<br> 
</p>");

define("Market_Making_info", "<p>The Tokenncoin team members are international experts in digital asset market making 
and liquidity services. Tokenncoin merges in-house algorithmic trading bots, high frequency trading infrastructure 
and industry expertise to supply market making and liquidity services to the ecosystem of the cryptocurrency industry. 
Our goal lies at building a more efficient, stable and accessible market for our clients.</p><br />
<p>Building the markets depend on the following factors:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; Building scalable technology<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; We focus on building a market responsibly<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Transparent and abundant data<br> 
&nbsp; <i class=\"bulletright\"></i> &nbsp; Risk management</p><br />
<p>Our main focus includes:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; Being online 24/7 in the market, making sure our clients have a liquid 
and healthy ecosystem.<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Providing our clients with transparent data for you to follow the 
operations and the general health of your market.<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Being an advisor to our clients and giving our support on exchange 
listing and tailor-made developments according to our clients’ token economics.
</p>");

define("Exchanges_info", "<p>Listing on exchanges is a very big milestone for your cryptocurrency project. Listing is 
never easy because of the difference in requirements from various exchanges. Listing strategies are different for each project.</p>
<p>Some projects choose to list on an exclusive exchange, and some projects opt for multiple exchanges listing. Tokenncoin 
aims to help clients execute their listing strategy and collaborate with our partner exchanges to make listing more successful for clients.</p>
<p>Tokenncoin is one of the most established platforms go to for help with listing your tokens or coins. We’ve worked 
with many new projects and we know the problems that new projects face. One of the biggest hurdles they need to 
overcome is finding a way to allow their users to openly exchange and trade a newly launched token. It’s no small feat 
to get listed.</p> <p>We have experience helping teams of all sizes small to large get their tokens listed on exchanges. We have the 
experience, technical expertise and exchange connections to help make this process of getting listed on exchanges 
quick and easy.</p>");

define("ICO_STO_info", "<p>Starting your ICO and STO will make your project face many difficulties. Launch your ICO and STO with us!
We have teams with people who are very exceptional working dedicatedly for making your ICO or STO launching very successful.</p>
<p>The ICO or STO stage is a very crucial part of any cryptocurrency project. This is the way that the project is 
getting support and funding from their supporters.</p> <p>By launching your ICO or STO, you are presenting the value of your cryptocurrency project to potential supporters.
Once you are able to reach your audience, they will review your project to see if it is a good project before they can 
decide to spend money in purchasing the tokens or coins that your project is making. There are many factors to be 
considered when reviewing your ICO and STO. Our company specializes in taking care of these challenges for our clients.</p>");

define("Drop_message", "DROP YOUR MESSAGE HERE");
define("Contact_name", "Name");
define("Contact_email", "Email");
define("Contact_subject", "Subject");
define("Contact_message", "Your Message");
define("Contact_address", "Address");
define("Contact_callUs", "Call Us");
define("Contact_mailUs", "Mail Us");

