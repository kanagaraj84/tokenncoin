<?php
define("connect_with_crypto", "CONNECT WITH CRYPTO");
define("title", "Token'n Coin ");
define("HOME_PAGE", "主页");
define("Tokenncoin-Content", "主页");
define("Company", "公司");
define("Tokenncoin", "<p>Tokenncoin是中东地区最好的数字货币咨询公司。为了帮助数字货币项目取得成功，Tokenncoin与最好的区块链开发人员，营销专家，业务开发和招聘人员合作，帮助每个客户实现数字货币项目的目标。 </p>

              <p>我们认为区块链技术是一个创新产业。 我们的公司业务是支持区块链项目。 
通过最大限度地发挥每个区块链项目的潜力，我们支持区块链行业。 
我们有能力和潜力探索和分享客户为各自社区提供的价值。
              </p>

              <p>我们的目标是将这些区块链项目连接到一个社区，该社区支持并相信他们为区块链世界提供的价值和贡献。
这是我们公司为正在制作自己的区块链项目的潜在客户提供的价值。  </p>
			   ");

define("Exchanges", "交易所上市 ");
define("Listing_on", "在交易所上市是数字货币项目的一个非常重要的里程。 由于各种交易所的要求不同，上市绝非易事。 每个项目的上市策略都不同。 有些项目选择在独家交易所上市，有些项目选择上好几家交易所。 Tokenncoin旨在帮助客户执行你们的上市策略，并与我们的合作伙伴（交易所）合作，使客户能上市成功。");
define("Services", "服务");
define("Marketing", "营销");
define("Marketing_services", "数字货币项目的营销服务，包括规划，品牌推广，设计，在线广告，社交媒体营销，电子邮件营销和内容营销 ");
define("Development","发展");
define("All_types", "为客户提供各种类型的开发，包括区块链开发，令牌和硬币创建，网站开发，Android和iOS开发以及更多IT解决的方案。");
define("LegalAssistance", "法律援助");
define("Our_goal", "我们的目标是确保客户的数字货币项目遵守监管标准。 数字货币项目需要遵循相关管理机构管辖范围内的指导原则");
define("Crypto_Advisor_Placement", "数字顾问");
define("Crypto_Advisor", "数字顾问");
define("Cryptocurrency_adisors", "数字货币顾问对数字货币项目的成功至关重要。 每个项目都需要一个可靠且经验丰富的数字货币顾问来帮助你们的需求。");
define("Market_Making", "造市商 ");
define("We_build", "我们可以通过增加流动性，填补订单中的空白，创建交易量，扩大紧缩和清算令牌来帮助您的项目。我们还可以根据客户需求建议个性化策略。");
define("SEO", "SEO");
define("Search_Engine", "搜索引擎优化是有机优化您的网站的做法，通过使用关键字在有机搜索引擎结果中获得更多可见性。");
define("SMM", "SMM");
define("Social_Media", "社交媒体营销是使用社交媒体来获得品牌意识的实践。 我们在数字行业中最好的社交媒体营销人员的帮助下采用各种社交媒体营销技术。");
define("Community_management", "社区管理");
define("It_is_very", "为任何数字货币项目建立一个快乐成长的社区非常重要。 当社区提供支持时，所有企业都会获得成功。");
define("ICO_Listing", "上市和评级");
define("Listing_your", "列出您的ICO并获得良好的评价是一种非常重要的营销方法，可以提高您的品牌声誉。 我们确保我们的客户列在最好的ICO网站上，并获得他们项目的最佳评论。");
define("Content_Marketing", "内容营销 ");
define("Making_content", "制作对目标受众非常有帮助的内容至关重要。 我们拥有内容营销方面的专家，可以为我们的客户创造出色的内容。");
define("Paid_Advertising", "付费广告 ");
define("We_provide_p", "我们为客户提供付费广告解决方案。 付费广告可以帮助您覆盖许多目标受众，以了解您的项目。");
define("Native_Advertising", "原生广告");
define("We_provide", "我们提供在各种着名的数字货币网站上发布原生广告的解决方案。 原生广告也有可能覆盖广泛的受众群体，具体取决于网站流量。");
define("PR_Marketing", "公关营销");
define("Press_releases", "新闻稿是数字货币营销的重要组成部分。 由于人们重视信息，我们为客户提供新闻稿撰写和出版服务。");
define("Influencer_Marketing_l", "影响者营销");
define("Influencer_Marketing", "影响者营销包括与有影响力的人建立业务关系，并与许多活跃的粉丝一起宣传我们客户的项目。");
define("Bounty_Program", "赏金计划咨询");
define("A_great_way", "与观众互动的好方法是通过赏金计划。 这将提高人们对您品牌的认知度并吸引支持者。");
define("Airdrop_Campaign", "空投运动咨询");
define("Airdrop_campaigns", "空投活动也是一种有效的营销策略。 通过向参与者发放免费硬币，项目可以增加他们的粉丝数量。");
define("Email_Marketing", "电子邮件营销");
define("Marketing_through", "通过电子邮件进行营销是新媒体中最有效的策略之一。 我们为电子邮件列表和群发电子邮件营销服务提供增长策略");
define("Video_Marketing", "视频营销");
define("Connecting_with", "与观众联系最好通过视频完成。 使用视频作为媒介是对您的营销工作的一个很好的投资。");
define("Custom_Blockchain_Solution", "定制区块链解决方案");
define("We_develop_blockchain", "我们根据每个客户的需求开发区块链解决方案，提供广泛的服务。 我们根据项目提供任何类型的需求解决方案。 这包括硬币或令牌创建以及任何形式的区块链开发。 这还包括钱包的开发。");
define("Smart_Contract_Service", "智能合约服务");
define("We_develop", "我们基于以太坊开发智能合约解决方案。 这包括制作适合客户要求的ERC20代币或任何形式的智能合约。");
define("Web_Development", "开发");
define("An_engaging", "对于任何区块链项目而言，具有吸引力且响应迅速的网站是必不可少的。 如果您需要一个网站，我们可以从头开发一个或修改您现有的网站，以便更好地为您服务。");
define("Mobile_App_Development", "移动应用开发");
define("Mobile_device", "移动设备的使用率不断增长。 这也是与观众互动的好方法。 这是任何数字货币项目的必要条件。");
define("ICO_STO_Launching", "ICO/STO 启动");
define("Starting_your", "启动ICO和STO是个非常困难的问题。 与我们帮助你们一起推出你们的ICO和STO！ 我们有非常出色的团队成员，致力于使您的ICO或STO发布成功。");
define("community_management", "Community management");
define("Review", "评论");
define("We_make", "我们确保根据项目为世界带来的质量，为您的项目提供最合适的审核人员");
define("Agency_Sign_Up", "机构注册");
define("We_are", "我们正在为您提供最好的代理商，这些代理商可以为您的ICO或STO做出巨大贡献。");
define("Token_Sales", "货币销售");
define("We_work_to_increase", "我们致力于提高您的ICO或STO的知名度并处理销售。 令牌销售极大地影响了项目的质量，因为在那里您可以获得初始投资以使项目更加成功。");
define("Blog", "博客");
define("Find_Tokenncoin", "在Tokenncoin网站的这一部分中能查到Tokenncoin最新的新闻和促销活动");
define("ico_sto_h", "我们提供ICO和STO启动服务。我们的服务可以帮助您在ICO或STO中取得更大成功。我们的服务包括STO和ICO审核，代理商注册和令牌销售。  ");
define("exchange_listing_h", "在交易所上面列出是数字货币项目的最大目标。通过与顶级交易所和现有合作伙伴关系，我们可以更轻松地为客户提供这方面的服务。");

//newly added constants
define("HomeSlide1_c1", "<span>Advanced</span> marketing solutions for blockchain companies");
define("HomeSlide1_c2", "The best marketing strategies that aligns with the goals of your business are within your grasp.");
define("ReadMore", "Read More");
define("HomeSlide2_c1", "<span>Work</span>toward the success of your cryptocurrency project");
define("HomeSlide2_c2", "We Offering Digital Marketing Solutions that can Tailored <br> to Suit any Budget and Business.");
define("HomeSlide3_c1", "<span>Wide</span>range of services all in one place");
define("HomeSlide3_c2", "We can help you with any type of blockchain related concern especially when it comes to 
tokens and coins.");

define("Digital_Marketing", "Digital Marketing");
define("Digital_Marketing_info", "Digital marketing encompasses all marketing efforts that use an electronic device or 
the internet.");
define("company_info", "Our company specializes in digital marketing and development services for clients in the 
cryptocurrency industry.");
define("Blockchain", "Blockchain Development");
define("Blockchain_info", "We developed decentralized, distributed and public digital ledgers that are used to record 
transactions across many computers.");

define("whyUs", "Why work with us?");
define("Best_Marketing", "Best Marketing Solutions");
define("Best_Marketing_info", "We have teams of experts with years of experience in marketing for the cryptocurrency industry.");
define("Latest_Tech", "Latest Technologies in Blockchain");
define("Latest_Tech_info", "Our blockchain specialists can apply the most recent technologies for developing your project.");
define("Scalable_Solutions", "Scalable Solutions");
define("Scalable_Solutions_info", "Our services include a wide variety and can easily adapt to our client needs.");

define("OurNews", "OUR NEWS");
define("OurNews_info", "We help you generate high-quality online sales leads by implementing highly structured, 
persuasive Internet marketing campaigns. ");

define("FAQ", "FAQ");
define("digital_marketing", "What is digital marketing?");
define("digital_marketing_info", "Digital marketing involves marketing to people using Internet-connected electronic 
devices, namely computers, smartphones and tablets. Digital marketing focuses on channels such as search engines, 
social media, email, websites and apps to connect with prospects and customers.");
define("business_benefit", "Would my business benefit from digital marketing?");
define("business_benefit_info", "Definitely. Though companies in many business categories continue to approach digital 
marketing with skepticism, avoiding digital marketing denies your business access to the media the majority of 
consumers turn to first and at all hours of the day.");
define("content_marketing", "What is content marketing?");
define("content_marketing_info", "Content marketing is the creation and distribution of educational and/or entertaining 
information assets for the purpose of generating brand awareness, traffic, leads and sales. Marketing content is 
generally free and does not explicitly promote your brand as an ad would.");
define("need_blog", "Do I need a blog?");
define("need_blog_info", "Email, social media, search and other elements of your digital marketing mix will depend on 
delivering useful content. Though video outlets, podcasts, and media galleries present other options, a blog is by far 
the most used tactic for distributing marketing content on a channel over which you have complete control.");
define("content_create", "What content should we create?");
define("content_create_info", "There can be no all-encompassing answer to this question except to say you should create 
content buyers will find relevant and useful. Leading candidates include blog posts, articles, ebooks, infographics, 
microsites, videos, courses, case studies, newsletters, visuals and various forms of interactive content.");

define("Subscribe", "Subscribe");
define("Subscribe_info", "Subscribe to our newsletter to receive exclusive offers and the latest news on our services.");

define("ContactUs", "Contact Us");
define("Useful_Links", "USEFUL LINKS");
define("Corporate_Office", "Corporate Office");
define("Addresslabel", "Address: ");
define("Address", "DUBAI, Business Bay, One by Omniyat Tower, office -1104");
define("Telephonelabel", "Telephone: ");
define("Telephone", "+971 4 874 8165");
define("Find_Map", "Find Us On Map");


define("Development_info", "<p>We are providing end-to-end cryptocurrency solutions to the clients. 
We are also creating scalable systems that support fast transactions. We created cryptocurrency systems for enterprises 
and startups based on blockchain technology.</p>
<p>Our services include cryptocurrency coins and token development, wallet development and smart contract development. 
We also provide custom blockchain solutions that can be flexible according to client needs.</p>
<p>We develop blockchain solutions with a very wide range of services based on every client’s needs. We have solutions for 
any type of requirement depending on the project. This includes coin or token creation and any form of blockchain 
development. This also includes development for wallets. </p>");

define("legal_info", "<p>Initial Coin Offerings or ICOs have increased in number. Aside from the Howey Test, there are various multifaceted concerns to consider when performing an ICO.
 With our wide-ranging knowledge in the industry of blockchain, we are capable of tailoring a very comprehensive strategy to ensure a successful
ICO launch / Token Sale from a legal standpoint.</p> <br />
<p>Our legal services include:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; Corporate Structure<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Jurisdiction Selection<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Regulatory Compliance<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; KYC/AML Framework<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Offering Memorandums and Purchase Agreements (including SAFTs)<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Legal Opinion on Utilities and Securities Token<br>
</p><br />
<p>Cryptocurrency trading volumes have increased worldwide. Because of these, the number
    of cryptocurrency exchange disputes also naturally gets bigger.
    These disputes are more common among cryptocurrency exchanges that strictly handle
    alternative coins (alt-coins) and are not accepting fiat currencies. </p><br />
<p>Most common disputes include: </p>
<p>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Cryptocurrency Withdrawal Issues<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Cryptocurrency Deposit Issues<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Account Ban<br>
    &nbsp; <i class=\"bulletright\"></i> &nbsp; Asset Freeze<br>
</p>");

define("Marketing_info", "<p>Tokenncoin delivers world-class marketing solutions for blockchain companies. Reaching both
 crypto and traditional audiences, Tokenncoin executes proven strategy to drive mass awareness of your project. With 
 awareness comes adoption and conversation, generating goal-oriented token sales.</p>
<p> Building up your platform's user base takes center stage when Tokenncoin applies our sophisticated marketing 
strategies across multifaceted digital channels to create strong month-over-month growth.</p>
<p>Tokenncoin team members are experts in marketing and are experienced in the leading industry projects. We can 
easily connect our clients’ projects to both the cryptocurrency community and the business to business sector 
working towards the development of blockchain.</p>");

define("Crypto_Advisor_info", "<p>Tokenncoin is rewriting the way we organize, and will continue to improve the lives 
of many clients and their target audience who are enthusiasts and participants in the cryptocurrency industry.</p>
<p>Emerging economic paradigms are rapidly transforming the way people trade. Innovative figures of value are already 
being developed. From the utmost beginning to the launching, part of our focus is on connecting advisors to the top 
cryptocurrency projects and creating meaningful impact.</p>
<p>We work with projects that have very high social value and have the capability to change people’s lives. If you are 
looking to top-notch advisors for your cryptocurrency project, we are a leading global agency that can connect you with 
the right people.</p> <p>We are offering different type of advisors for your crypto project:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; ICO advisors<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; STO advisors<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Legal advisors<br> 
</p>");

define("Market_Making_info", "<p>The Tokenncoin team members are international experts in digital asset market making 
and liquidity services. Tokenncoin merges in-house algorithmic trading bots, high frequency trading infrastructure 
and industry expertise to supply market making and liquidity services to the ecosystem of the cryptocurrency industry. 
Our goal lies at building a more efficient, stable and accessible market for our clients.</p><br />
<p>Building the markets depend on the following factors:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; Building scalable technology<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; We focus on building a market responsibly<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Transparent and abundant data<br> 
&nbsp; <i class=\"bulletright\"></i> &nbsp; Risk management</p><br />
<p>Our main focus includes:</p>
<p> &nbsp; <i class=\"bulletright\"></i> &nbsp; Being online 24/7 in the market, making sure our clients have a liquid 
and healthy ecosystem.<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Providing our clients with transparent data for you to follow the 
operations and the general health of your market.<br>
&nbsp; <i class=\"bulletright\"></i> &nbsp; Being an advisor to our clients and giving our support on exchange 
listing and tailor-made developments according to our clients’ token economics.
</p>");

define("Exchanges_info", "<p>Listing on exchanges is a very big milestone for your cryptocurrency project. Listing is 
never easy because of the difference in requirements from various exchanges. Listing strategies are different for each project.</p>
<p>Some projects choose to list on an exclusive exchange, and some projects opt for multiple exchanges listing. Tokenncoin 
aims to help clients execute their listing strategy and collaborate with our partner exchanges to make listing more successful for clients.</p>
<p>Tokenncoin is one of the most established platforms go to for help with listing your tokens or coins. We’ve worked 
with many new projects and we know the problems that new projects face. One of the biggest hurdles they need to 
overcome is finding a way to allow their users to openly exchange and trade a newly launched token. It’s no small feat 
to get listed.</p> <p>We have experience helping teams of all sizes small to large get their tokens listed on exchanges. We have the 
experience, technical expertise and exchange connections to help make this process of getting listed on exchanges 
quick and easy.</p>");

define("ICO_STO_info", "<p>Starting your ICO and STO will make your project face many difficulties. Launch your ICO and STO with us!
We have teams with people who are very exceptional working dedicatedly for making your ICO or STO launching very successful.</p>
<p>The ICO or STO stage is a very crucial part of any cryptocurrency project. This is the way that the project is 
getting support and funding from their supporters.</p> <p>By launching your ICO or STO, you are presenting the value of your cryptocurrency project to potential supporters.
Once you are able to reach your audience, they will review your project to see if it is a good project before they can 
decide to spend money in purchasing the tokens or coins that your project is making. There are many factors to be 
considered when reviewing your ICO and STO. Our company specializes in taking care of these challenges for our clients.</p>");

define("Drop_message", "DROP YOUR MESSAGE HERE");
define("Contact_name", "Name");
define("Contact_email", "Email");
define("Contact_subject", "Subject");
define("Contact_message", "Your Message");
define("Contact_address", "Address");
define("Contact_callUs", "Call Us");
define("Contact_mailUs", "Mail Us");