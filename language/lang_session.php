<?php
session_start();
$language = "";
$key = "";
$val = "";

// Set Language variable
if(isset($_GET['lang']) && !empty($_GET['lang'])){
  $_SESSION['lang'] = $_GET['lang'];

  if(isset($_SESSION['lang']) && $_SESSION['lang'] != $_GET['lang']){
    echo "<script type='text/javascript'> location.reload(); </script>";
  }
}


foreach ($_SESSION as $key=>$val)

  $language = $key."=".$val;

if ($key==null && $val==null){
    include('lang_en.php');
}
if ($language=="lang=en"){
  include('lang_en.php');
}

if ($language=="lang=cn"){
  include('lang_cn.php');
}

if ($language=="lang=kr"){
  include('lang_kr.php');
}

if ($language=="lang=ru"){
  include('lang_ru.php');
}
?>

