<!-- Navigation -->
  
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
       <a href="https://tokenncoin.com/index.php">  <img class="img-responsive" src="img/token_coin_logo.svg" height="45"></a>

      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive" style ="font-family: 'Poppins';">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="https://tokenncoin.com/index.php"><?= HOME_PAGE ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="https://tokenncoin.com/crypto_marketing_company.php"><?= Company ?></a>
			
          </li>

          <li class="nav-item dropdown" >
            <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link" ><?= Services?> <b class="caret"></b></a>
            <ul class="dropdown-menu" style="border:none;">
             			<li><a href="crypto_development_services.php" class="nav-link" style="color:#343a40;"><?= Development?></a></li>
                           <li><a href="legal_assistance.php" class="nav-link" style="color:#343a40;"><?= LegalAssistance ?></a></li>
              <li><a href="https://tokenncoin.com/crypto_marketing_services.php" class="nav-link" style="color:#343a40;"><?= Marketing?></a></li>
              <li><a href="https://tokenncoin.com/crypto_advisor.php" class="nav-link" style="color:#343a40;"><?= Crypto_Advisor ?></a></li>
              <li><a href="https://tokenncoin.com/market_making.php" class="nav-link" style="color:#343a40;"><?= Market_Making ?></a></li>
            </ul>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="exchange_listing.php"><?= Exchanges ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="https://tokenncoin.com/ico_launching_services.php"><?= ICO_STO_Launching ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#"><?= Blog ?></a>
          </li>
       <li class="nav-item">
       <!-- <a class="nav-link" href="#">
			   <form method='get' action='' id='form_lang' >
                  <select name='lang'  onchange='changeLang();' class="nav-item" style="">
                   <option value='en' <?php //if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'en'){ echo "selected"; } ?> style="background:none;color:#222;">Eng</option>
                   <option value='cn' <?php //if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'cn'){ echo "selected"; } ?>  style="background:none;color:#222;" >Cn</option>
                   <option value='kr' <?php //if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'kr'){ echo "selected"; } ?>  style="background:none;color:#222;" >Kr</option>
             
                 </select>
               </form> 
			   </a>-->
 </li>
        </ul>
      </div>
    </div>
  </nav>
  <script>
   function changeLang(lang){
  
    document.getElementById('form_lang').submit();
  }
  window.onload = function(){

    document.body.classList.add('active');

  }

  function exit(){
    document.body.classList.remove('active');
    var x = document.getElementById("popup1");
    x.style.display = "none";
  }
</script>