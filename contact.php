<?php include('language/lang_session.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= ContactUs ?></title>
    <!-- Stylesheets -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<body>
<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>
    <!-- Main Header / Header Style Five-->
    <header class="main-header header-style-two">
        <?php include "header.php"; ?>
    </header>
    <!--End Main Header -->
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/contact-us.jpg);">
    </section>
    <!--End Page Title-->

    <!--Contact Section-->
    <section class="contact-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Contact Form Column-->
                <div class="contact-form-column col-md-8 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <h2><?=Drop_message ?></h2>
                        <!--Contact Form-->
                        <div class="contact-form">
                            <form method="post" action="sendemail.php" id="contact-form">
                                <div class="row clearfix">
                                    <div class="column col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" name="username" placeholder="<?=Contact_name?>*" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="<?=Contact_email ?>*" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="subject" placeholder="<?=Contact_subject ?>" required>
                                        </div>
                                    </div>
                                    <div class="column col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <textarea name="message" placeholder="<?=Contact_message ?>..."></textarea>
                                        </div>
                                    </div>
                                    <div class="column btn-column col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <button class="theme-btn btn-style-one" type="submit" name="submit-form">Submit Now</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!--Info Column-->
                <div class="contact-info-column col-md-4 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="lower-box">
                            <ul class="info-list">
                                <li><span class="icon"><img src="images/icons/address-icon.png" alt="" /></span>
                                    <strong><?=Contact_address ?>:</strong> DUBAI, Business Bay, One by Omniyat Tower, office -1104</li>
                                <li><span class="icon"><img src="images/icons/phone-icon.png" alt="" /></span>
                                    <strong><?=Contact_callUs ?>:</strong><br> +971 4 874 8165</li>
                                <li><span class="icon"><img src="images/icons/mail-icon.png" alt="" /></span>
                                    <strong><?=Contact_mailUs ?>:</strong> Support@tokenncoin.com</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--End Contact Section-->

    <!--Map Section-->
    <section class="map-section">
        <!--Map Outer-->
        <div class="map-outer">
            <!--Map Canvas-->
            <div class="map-canvas"
                 data-zoom="12"
                 data-lat="-37.817085"
                 data-lng="144.955631"
                 data-type="roadmap"
                 data-hue="#ffc400"
                 data-title="Tokenncoin"
                 data-icon-path="images/icons/map-marker.png"
                 data-content="One By Omniyat, Dubai<br><a href='mailto:info@tokenncoin.com'>info@tokenncoin.com</a>">
            </div>
        </div>
    </section>
    <!--Map Section-->

    <!--Main Footer-->
    <footer class="main-footer">
        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    <!--Column-->
                    <div class="column col-md-7 col-sm-12 col-xs-12">
                        <div class="footer-widget links-widget">
                            <div class="footer-title">
                                <h2><?=Useful_Links ?></h2>
                            </div>
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="widget-column col-md-6 col-sm-6 col-xs-12">
                                        <ul class="footer-list">
                                            <li><a href="crypto_marketing_company.php<?=$language ?>"><?= Company ?></a></li>
                                            <li><a href="crypto_development_services.php<?=$language ?>"><?= Development ?></a></li>
                                            <li><a href="crypto_marketing_services.php<?=$language ?>"><?= Marketing ?></a></li>
                                            <li><a href="crypto_advisor.php<?=$language ?>"><?= Crypto_Advisor ?></a></li>
                                            <li><a href="market_making.php<?=$language ?>"><?= Market_Making ?></a></li>
                                        </ul>
                                    </div>
                                    <div class="widget-column col-md-6 col-sm-6 col-xs-12">
                                        <ul class="footer-list">
                                            <li><a href="legal_assistance.php<?=$language ?>"><?= LegalAssistance ?></a></li>
                                            <li><a href="exchanges_listing.php<?=$language ?>"><?= Exchanges ?></a></li>
                                            <li><a href="ico_launching_services.php<?=$language ?>"><?= ICO_STO_Launching ?></a></li>
                                            <li><a href="blog.php<?=$language ?>"><?= Blog ?></a></li>
                                            <li><a href="contact.php<?=$language ?>"><span class="icon"></span><?=ContactUs ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                    <div class="column col-md-5 col-sm-12 col-xs-12">
                        <div class="footer-widget office-widget">
                            <div class="footer-title">
                                <h2><?=Corporate_Office ?></h2>
                            </div>
                            <div class="widget-content">
                                <div class="single-item-carousel owl-carousel owl-theme">

                                    <div class="office-info">
                                        <ul>
                                            <li><strong><?=Addresslabel ?></strong></li>
                                            <li>DUBAI, Business Bay, One by Omniyat Tower, office -1104 </li>
                                            <li><a href="index.php#map-home"><?=Find_Map ?></a></li>
                                            <li><strong><?=Telephonelabel ?></strong></li>
                                            <li>+971 4 874 8165</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Copyright-->
        <div class="copyright">&copy; tokenncoin.com 2019. All Rights Reserved.</div>
    </footer>
    <!--Main Footer-->

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/validate.js"></script>
<script src="js/appear.js"></script>
<script src="js/script.js"></script>
<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="js/map-script.js"></script>
<!--End Google Map APi-->

</body>
</html>